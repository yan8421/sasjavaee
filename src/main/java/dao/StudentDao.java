package dao;

import model.Student;

import java.sql.SQLException;
import java.util.List;

/*
*  对student进行增删改查
* */
public interface StudentDao {
    List<Student> getAllStudent() throws SQLException;


    List<Student> getStudentByName(String name);

    boolean saveStudent(Student s);

    Student getStudentByNo(String stuno);

    List<Student> getStudentByPaging(int currentPage , int pageSize);

    long totalRecords();

    boolean deleteStudent(String stuno);

    boolean changeStudent(Student s);

}
