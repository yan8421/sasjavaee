//package dao.impl;
//
//import dao.StudentDao;
//import model.Student;
//import org.apache.commons.dbutils.handlers.MapHandler;
//import util.C3p0Util;
//import org.apache.commons.dbutils.QueryRunner;
//import org.apache.commons.dbutils.handlers.BeanListHandler;
//import org.apache.commons.dbutils.handlers.MapListHandler;
//
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//public class StudentDaoImpl2
//implements StudentDao
// {
//    private QueryRunner queryRunner = null;
//
//    @Override
//    public List<Student> getAllStudent() throws SQLException {
//        String sql = "select * from tbl_student";
//        QueryRunner queryRunner = new QueryRunner(C3p0Util.getConnection());
//        List<Student> students = queryRunner.query(sql, new BeanListHandler<Student>(Student.class));
//        return students;
//
//    }
//
//    @Override
//    public List<Student> getStudentByName(String name) {
//        return null;
//    }
//
//    @Override
//    public boolean saveStudent(Student s) {
//        return false;
//    }
//
//    @Override
//    public Student getStudentByNo(String stuno) {
//        return null;
//    }
//
//    @Override
//    public List<Student> getStudentByPaging(int currentPage, int pageSize) {
//        String sql = "select * from tbl_student where stuno limit ? , ?";
//        int start = (currentPage - 1)*pageSize;
//        List<Object> params = new ArrayList<>();
//        params.add(start);
//        params.add(pageSize);
//        queryRunner = new QueryRunner(C3p0Util.getConnection());
//        try {
//            List<Student> students = queryRunner.query(sql, new BeanListHandler<Student>(Student.class), new Object[]{start, pageSize});
//            return students;
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public long totalRecords() {
//        String sql = "select count(*) as count from tbl_student";
//        queryRunner = new QueryRunner(C3p0Util.getConnection());
//        try {
//            List<Map<String, Object>> query = queryRunner.query(sql, new MapListHandler());
//            return (long)query.get(0).get("count");
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        return -1;
//
//    }
//
//    @Override
//    public boolean deleteStudent(String stuno) {
//        String sql = "delete from tbl_student where stuno = ?";
//List<Object> params = new ArrayList<>();
//        params.add(stuno);
//
//        queryRunner = new QueryRunner(C3p0Util.getConnection());
//
//        try {
//            queryRunner.update(sql , stuno);
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        return false;
//    }
//
//    @Override
//    public boolean changeStudent(Student s) {
//        return false;
//    }
//}
