package util;

import java.io.*;
import java.util.UUID;

public class FileUtil {
    public static String getStringFromInputStream(InputStream inputStream) throws IOException{
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String buffer = "";
        StringBuffer resultStr = new StringBuffer();
        while ((buffer = bufferedReader.readLine()) != null){
            resultStr.append(buffer);
        }
        return resultStr.toString();
    }

    public static String getRandomFileName(String fileName){
        int index = fileName.lastIndexOf(".");
        String substring = fileName.substring(index + 1);
        return fileName.substring(0 , index) + "-" + UUID.randomUUID() + "." + substring;
    }

    public static void main(String[] args) {
        System.out.println(FileUtil.getRandomFileName("test.jpg"));
    }
}
