package model;

public class SearchStudent {
    private String searchText;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public SearchStudent(String searchText) {
        this.searchText = searchText;
    }

    public SearchStudent() {
    }

    @Override
    public String toString() {
        return "SearchStudent{" +
                "searchText='" + searchText + '\'' +
                '}';
    }
}
