package servlet;

import com.google.gson.Gson;
import dao.UserDao;
import dao.impl.StudentDaoImpl;
import model.Student;
import model.User;
import util.requestReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/AddStudentServletForAndroid")
public class AddStudentServletForAndroid extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset = UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        String studentJson = new requestReader().ReadAsChars(request);
        System.out.println(studentJson);
        Gson gson = new Gson();
        Student student = gson.fromJson(studentJson, Student.class);
        boolean b = new StudentDaoImpl().saveStudent(student);
        System.out.println(b);
        String s = gson.toJson(b);
        writer.write(s);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
