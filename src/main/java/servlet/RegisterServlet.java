package servlet;

import dao.UserDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/RegisterServlet")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset = UTF-8");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = new User(username, password);
        UserDao userDao = new UserDao();
        boolean b = userDao.saveUser(user);
        PrintWriter writer = response.getWriter();
        if (b){
            writer.write("<script>alert('注册成功');location = 'Login.html'</script>");
            //request.getRequestDispatcher("Login.html").forward(request , response);
        }else {
            writer.write("<script>alert('注册失败');location = 'Regist.html'</script>");
            //request.getRequestDispatcher("Regist.html").forward(request , response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
