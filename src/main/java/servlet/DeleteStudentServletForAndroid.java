package servlet;

import com.google.gson.Gson;
import dao.impl.StudentDaoImpl;
import util.requestReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/DeleteStudentServletForAndroid")
public class DeleteStudentServletForAndroid extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset = UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        String stuNo = request.getParameter("stuNo");
        System.out.println(stuNo);
        Gson gson = new Gson();
        boolean b = new StudentDaoImpl().deleteStudent(stuNo);
        System.out.println(b);
        String s = gson.toJson(b);
        writer.write(s);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
