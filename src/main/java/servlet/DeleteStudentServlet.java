package servlet;

import com.google.gson.Gson;
import dao.impl.StudentDaoImpl;
import model.Student;
import util.requestReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/DeleteStudentServlet")
public class DeleteStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset = UTF-8");
        PrintWriter writer = response.getWriter();
        String stuno = request.getParameter("stuno");
        if (stuno == null){
            writer.write("<script>alert('该生不存在');location = 'ListStudents.jsp'</script>");
            //request.getRequestDispatcher("ListStudents.jsp").forward(request , response);
        }else {
            StudentDaoImpl studentDao = new StudentDaoImpl();
            boolean b = studentDao.deleteStudent(stuno);
            if (b){
                writer.write("<script>alert('删除成功');location = 'ListStudents.jsp'</script>");
                //request.getRequestDispatcher("ListStudents.jsp").forward(request , response);
            }else {
                writer.write("<script>alert('删除失败');location = 'ListStudents.jsp'</script>");
                //request.getRequestDispatcher("ListStudents.jsp").forward(request , response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
