package servlet;

import dao.impl.StudentDaoImpl;
import model.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/ChangeStudentServlet")
public class ChangeStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset = UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        String stuno = request.getParameter("stuno2");
        String stuname = request.getParameter("stuname");
        String classes = request.getParameter("classes");
        String gender = request.getParameter("gender");
        String department = request.getParameter("department");
        String tel = request.getParameter("tel");
        String dormno = request.getParameter("dormno");
        String photopath = "";
        if (gender.equals("男")){
            photopath = "img/man.jpg";
        }else {
            photopath = "img/women.jpg";
        }
        Student student = new Student(stuno, stuname, classes, gender, department, tel, dormno, photopath);
        boolean b = new StudentDaoImpl().changeStudent(student);
        if (b){
            writer.write("<script>alert('修改成功');location = 'ListStudents.jsp'</script>");
            //request.getRequestDispatcher("ListStudents.jsp").forward(request , response);
        }else {
            writer.write("<script>alert('修改失败');location = 'updatestudent.jsp'</script>");
            //request.getRequestDispatcher("addstudents.jsp").forward(request , response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
