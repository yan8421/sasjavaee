package servlet;

import com.google.gson.Gson;
import dao.UserDao;
import model.User;
import util.requestReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/RegisterServletForAndroid")
public class RegisterServletForAndroid extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/json; charset = UTF-8");
        request.setCharacterEncoding("UTF-8");
        String userJson = new requestReader().ReadAsChars(request);
        System.out.println(userJson);
        Gson gson = new Gson();
        User user = gson.fromJson(userJson, User.class);
        System.out.println(user.toString());
        boolean b = new UserDao().saveUser(user);
        System.out.println(b);
        String s = gson.toJson(b);
        PrintWriter writer = response.getWriter();
        writer.write(s);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
