package servlet;

import com.google.gson.Gson;
import dao.impl.StudentDaoImpl;
import model.SearchStudent;
import model.Student;
import util.requestReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

@WebServlet(urlPatterns = "/SearchStudentServletForAndroid")
public class SearchStudentServletForAndroid extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset = UTF-8");
        request.setCharacterEncoding("UTF-8");
        String searchText = new requestReader().ReadAsChars(request);
        SearchStudent stuName = new Gson().fromJson(searchText, SearchStudent.class);
        System.out.println(stuName.getSearchText());
        List<Student> students = new StudentDaoImpl().getStudentByName(stuName.getSearchText());
        response.getWriter().write(new Gson().toJson(students));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
