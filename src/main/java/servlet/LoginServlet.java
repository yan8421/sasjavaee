package servlet;

import com.google.gson.Gson;
import dao.UserDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset = UTF-8");

        //1.获取用户提交的数据
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        //2.创建user
        User user = new User(username, password);
        //3.使用userDao来验证
        UserDao userDao = new UserDao();
        boolean result = userDao.checkLogin(user);
        PrintWriter writer = response.getWriter();
        //4.页面跳转
        if (result){
            //writer.write("<script>alert('登录成功');location = 'ListStudents.jsp'</script>");
            request.getRequestDispatcher("ListStudents.jsp?page=1").forward(request , response);

        }else {
            writer.write("<script>alert('登录失败');location = 'Login.html'</script>");
            //request.getRequestDispatcher("Login.html").forward(request , response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
