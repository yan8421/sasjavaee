package servlet;

import com.google.gson.Gson;
import dao.impl.StudentDaoImpl;
import model.Student;
import util.PageUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = "/GetStudentByPageServletForAndroid")
public class GetStudentByPageServletForAndroid extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        int page = Integer.parseInt(request.getParameter("page"));
        int pageSize = Integer.parseInt(request.getParameter("pageSize"));
        long totalRecords = new StudentDaoImpl().totalRecords();
        PageUtil pageUtil = new PageUtil(totalRecords, pageSize, page);
        List<Student> students = new StudentDaoImpl().getStudentByPaging(pageUtil.getCurrentPage() , pageSize);
        Gson gson = new Gson();
        String s = gson.toJson(students);
        PrintWriter writer = response.getWriter();
        writer.write(s);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
