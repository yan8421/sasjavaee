package servlet;

import com.google.gson.Gson;
import dao.impl.StudentDaoImpl;
import model.Student;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(urlPatterns = "/SearchStudentServlet")
public class SearchStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset = UTF-8");
        request.setCharacterEncoding("UTF-8");
        String studentName = request.getParameter("studentName");
        System.out.println(studentName);
        List<Student> studentByName = new StudentDaoImpl().getStudentByName(studentName);
        request.setAttribute("searchStudentResult" , studentByName);
        request.getRequestDispatcher("SearchStudentResult.jsp").forward(request , response);
        //request.getRequestDispatcher("test.jsp").forward(request , response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request , response);
    }
}
