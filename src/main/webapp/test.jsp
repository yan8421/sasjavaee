<%@ page import="java.util.List" %>
<%@ page import="model.Student" %><%--
  Created by IntelliJ IDEA.
  User: TZK
  Date: 2020/12/30
  Time: 19:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
    <%
        List<Student> students = (List<Student>)request.getAttribute("searchStudentResult");
        if (students != null && students.size() != 0){
            for (int i = 0 ; i < students.size() ; i++){
                Student student = (Student)students.get(i);
                out.print(student.toString());
            }
        }
    %>
</body>
</html>
