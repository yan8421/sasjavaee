<%@ page import="dao.StudentDao" %>
<%@ page import="model.Student" %>
<%@ page import="java.util.List" %>
<%@ page import="dao.impl.StudentDaoImpl" %>
<%@ page import="util.PageUtil" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="com.mysql.cj.ServerPreparedQuery" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>学生信息</title>
    <link rel="stylesheet" href="assets2/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets2/css/Data-Table-1.css">
    <link rel="stylesheet" href="assets2/css/Data-Table.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="assets2/css/styles.css">
</head>

<body>
<h1 style="text-align: center;margin: 0px auto;">学生信息管理系统</h1><br>
<hr>
<hr>
<a href="ListStudents.jsp" style="text-decoration: none; color: black">返回首页</a>
<hr>
<form action="SearchStudentServlet" method="post">
    请输入姓名<input type="text" id="studentName" name="studentName">
    <input type="submit" value="搜索" id="submit" name="submit">
</form>
<hr>
<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <td>学号</td>
        <td>姓名</td>
        <td>班级</td>
        <td>性别</td>
        <td>专业</td>
        <td>电话</td>
        <td>宿舍号</td>
        <td>照片</td>
        <td>操作</td>
    </tr>
    </thead>
    <tbody>
    <%
        List<Student> students = (List<Student>)request.getAttribute("searchStudentResult");
        if (students != null && students.size() != 0){
            for (Student s:students){
    %>
    <tr>
        <td><%=s.getStuNo()%></td>
        <td><%=s.getStuName()%></td>
        <td><%=s.getClasses()%></td>
        <td><%=s.getGender()%></td>
        <td><%=s.getDepartment()%></td>
        <td><%=s.getTel()%></td>
        <td><%=s.getDormNo()%></td>
        <td>
            <img src="<%=s.getPhotoPath()%>" style="width: 100px; height: 100px"/>
        </td>
        <td>
            <a href="updatestudent.jsp?stuno=<%=s.getStuNo()%>">修改</a>
            <a href="DeleteStudentServlet?stuno=<%=s.getStuNo()%>">删除</a>
        </td>
    </tr>
    <% }
    }
    %>

    </tbody>
</table>
<br>
<br>
<br>
<script src="assets2/js/jquery.min.js"></script>
<script src="assets2/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
</body>

</html>