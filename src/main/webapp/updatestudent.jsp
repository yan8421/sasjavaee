<%@ page import="java.lang.String" %>
<%@ page import="model.Student" %>
<%@ page import="dao.impl.StudentDaoImpl" %><%--
  Created by IntelliJ IDEA.
  User: TZK
  Date: 2020/12/25
  Time: 14:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改学生页面</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h1>修改学生信息</h1><hr>
<%
    String stuno = request.getParameter("stuno");
    Student student = null;
    if (stuno == null){
        out.print("<script>alert('未获取到学生信息!');location.href='ListStudents.jsp'</script>");
    }else {
        student = new StudentDaoImpl().getStudentByNo(stuno);

%>
<form action="ChangeStudentServlet" method="post" id="form1">


    请输入学号：<input type="text" id="stuno2" name="stuno2" value="<%=student.getStuNo()%>" disabled>   <br>
    请输入姓名：<input type="text" id="stuname" name="stuname" value="<%=student.getStuName()%>"><br>
    请选择班级：<select name="classes" id="classes">
    <option value="1808041">1808041</option>
    <option value="1808042">1808042</option>
    <option value="1808031">1808031</option>
    <option value="1808032">1808032</option>
</select><br>

    请选择性别：<select id="gender" name="gender">
    <%
        if (student.getGender().equals("男")){
    %>
    <option value="男" selected>男</option>
    <option value="女">女</option>
    <%
    }else {
    %>
    <option value="男" >男</option>
    <option value="女" selected>女</option>
    <%
        }
    %>
</select>
    <br>
    请选择系部：<select id="department" name="department">
    <option value="软件工程">软件工程</option>
    <option value="计算机应用">计算机应用</option>
</select><br>
    请输入电话：<input type="text" id="tel" name="tel" value="<%=student.getTel()%>"><br>
    请选择宿舍：<select id="dormno" name="dormno">
    <option value="3-101">3-101</option>
    <option value="4-304">4-304</option>
</select><br>
    <%
        }
    %>

    <script>
        function addSelected(ele , value){
            var options = ele.options;
            for (option of options){
                if (option.value == value){
                    option.selected = true;
                }
            }
        }
        var classes = document.getElementById("classes");
        addSelected(classes , "<%=student.getClasses()%>");
        var department = document.getElementById("department");
        addSelected(department , "<%=student.getDepartment()%>");
        var dormno = document.getElementById("dormno");
        addSelected(dormno , "<%=student.getDormNo()%>");
    </script>
    </div><button class="btn btn-light submit-button" type="button" id="submit_button">
        提交
    </button>
</form>
<hr>
<a href="ListStudents.jsp" style="text-decoration: none; color: black">返回首页</a>
<hr>

<script>
    var submit_button = document.getElementById("submit_button");
    var form1 = document.getElementById("form1");
    var stuno2 = document.getElementById("stuno2");
    submit_button.onclick = function (){
        stuno2.disabled = false;
        form1.submit();

    }
</script>

</body>
</html>
